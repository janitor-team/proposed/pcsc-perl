pcsc-perl (1.4.14-4) unstable; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 24 Mar 2018 23:52:18 +0100

pcsc-perl (1.4.14-3) unstable; urgency=medium

  * Use Vcs-Git: instead of Vcs-Svn:

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 22 Dec 2017 13:07:24 +0100

pcsc-perl (1.4.14-2) unstable; urgency=medium

  * Migrate the Debian packging from SVN to GIT
  * Upgrade to debhelper v10
  * Use hardening=+all
  * Standards-Version: 3.9.6 -> 3.9.8. No change needed

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 22 Dec 2017 12:58:50 +0100

pcsc-perl (1.4.14-1) unstable; urgency=medium

  * New upstream release
  * debian/control: update VCS-* fields to canonical form (thanks lintian)
  * debian/control: Standards-Version: 3.9.4 -> 3.9.6. No change needed.
  * debian/watch: add upstream signature verification

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 19 Nov 2015 17:26:36 +0100

pcsc-perl (1.4.13-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.9.1 -> 3.9.4. No change needed.
  * debian/compat: 7 -> 9. Get automatic hardening of compiled code.
  * debian/copyright: refer to a specific version of GPL

 -- Ludovic Rousseau <rousseau@debian.org>  Mon, 01 Apr 2013 12:44:59 +0200

pcsc-perl (1.4.12-1) unstable; urgency=low

  * New upstream release
  * Fix "GetStatusChange error after print" fixed upstream (Closes: #613722)

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 06 Mar 2011 20:26:21 +0100

pcsc-perl (1.4.11-3) unstable; urgency=low

  * upload to unstable

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 09 Feb 2011 23:18:33 +0100

pcsc-perl (1.4.11-2) experimental; urgency=low

  * debian/watch: use .bz2 instead of .gz extension

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 04 Dec 2010 15:53:19 +0100

pcsc-perl (1.4.11-1) experimental; urgency=low

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 27 Oct 2010 15:26:09 +0200

pcsc-perl (1.4.10-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.9.0 -> 3.9.1. No change needed.
  * debian/control: remove Conflicts: pcsc-tools (<= 1.2.4-1) since that
    package version is very old. Stable (lenny) has 1.4.14-1.

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 18 Aug 2010 23:16:50 +0200

pcsc-perl (1.4.9-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.8.3 -> 3.9.0. No change needed.
  * debian/source/format: use "3.0 (quilt)" format

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 30 Jun 2010 18:11:51 +0200

pcsc-perl (1.4.8-1) unstable; urgency=low

  * New upstream release
  * debian/control: 3.7.3 -> 3.8.3. No change needed
  * debian/rules: use dh
    Should fix problem with Perl 5.10
    http://lists.debian.org/debian-devel/2009/09/msg00556.html

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 23 Sep 2009 12:52:45 +0200

pcsc-perl (1.4.7-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   - Build-Depends: libpcsclite-dev (>= 1.3.2) instead of 1.3.2-1 to avoid a
   lintian warning build-depends-on-1-revision
   - use the Homepage: field
   - Standards-Version: 3.6.1 -> 3.7.3. No change needed
   - add Vcs-Svn: and Vcs-Browser: now the packaging is hosted on
     alioth/collab-maint
  * debian/{preinst,postrm,prerm,postinst}: remove "empty" files. lintian
    warning maintainer-script-empty
  * debian/copyright: add an explicit "Copyright" to avoid a
    copyright-without-copyright-notice lintian warning

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 26 Mar 2008 21:26:55 +0100

pcsc-perl (1.4.6-1) unstable; urgency=low

  * new upstream release
   - Closes: #413618: "pcsc-perl: FTBFS on GNU/kFreeBSD: tiny adjustment
     needed"

 -- Ludovic Rousseau <rousseau@debian.org>  Wed,  7 Mar 2007 21:35:04 +0100

pcsc-perl (1.4.5-1) unstable; urgency=low

  * new upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Fri,  2 Mar 2007 18:00:29 +0100

pcsc-perl (1.4.4-1) unstable; urgency=low

  * New upstream release
  * Build-Depends: libpcsclite-dev (>= 1.3.2-1) to have the support of
    extended APDU

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 12 Aug 2006 21:02:48 +0200

pcsc-perl (1.4.3-1) unstable; urgency=low

  * New upstream release
  * debian/{rules,compat}: remove DH_COMPAT=3 from rules and use level 4 in
    debian/compat

 -- Ludovic Rousseau <rousseau@debian.org>  Tue, 16 May 2006 21:59:45 +0200

pcsc-perl (1.4.2-1) unstable; urgency=low

  * new upstream release
  * debian/control: Section: libs -> perl

 -- Ludovic Rousseau <rousseau@debian.org>  Tue,  7 Sep 2004 21:37:30 +0200

pcsc-perl (1.4.1-2) unstable; urgency=low

  * debian/control:
   - build depends on pkg-config
   - change Section: from perl to lib

 -- Ludovic Rousseau <rousseau@debian.org>  Mon,  5 Jul 2004 11:13:03 +0200

pcsc-perl (1.4.1-1) unstable; urgency=low

  * new upstream release
  * debian/control: build depends on libpcsclite-dev (>= 1.2.9-beta4-1)
    instead of 1.2.9-beta2-1 so that `pkg-config --cflags libpcsclite` works
    as expected.

 -- Ludovic Rousseau <rousseau@debian.org>  Sun,  4 Jul 2004 18:45:46 +0200

pcsc-perl (1.4.0-1) unstable; urgency=low

  * new upstream release
  * debian/control: Depends on libpcsclite1

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 30 May 2004 22:37:57 +0200

pcsc-perl (1.3.1-1) unstable; urgency=low

  * new upstream release
  * debian/control: update Standards-Version from 3.5.10 to 3.6.1
  * debian/watch: upgrade to format V2

 -- Ludovic Rousseau <rousseau@debian.org>  Fri,  2 Apr 2004 20:44:52 +0200

pcsc-perl (1.3.0-1) unstable; urgency=low

  * new upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 17 Dec 2003 18:37:40 +0100

pcsc-perl (1.2.2-1) unstable; urgency=low

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Tue, 27 May 2003 21:45:20 +0200

pcsc-perl (1.2.1-2) unstable; urgency=low

  * debian/control: change Section: from interpreters to perl

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 25 May 2003 01:12:32 +0200

pcsc-perl (1.2.1-1) unstable; urgency=low

  * New upstream release
  * debian/control: Conflicts: pcsc-tools (<= 1.2.4-1) since this package uses
    the old naming scheme
  * debian/control: update Standards-Version from 3.5.9 to 3.5.10

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 25 May 2003 00:28:42 +0200

pcsc-perl (1.2.0-1) unstable; urgency=low

  * New upstream release
    new naming scheme Chipcard::PCSC instead of PCSC
  * debian/control: update Standards-Version from 3.5.6 to 3.5.9

 -- Ludovic Rousseau <rousseau@debian.org>  Fri,  9 May 2003 22:21:58 +0200

pcsc-perl (1.1.3-1) unstable; urgency=low

  * New upstream release
  * debian/control: update Standards-Version from 3.5.6 to 3.5.7

 -- Ludovic Rousseau <rousseau@debian.org>  Thu,  7 Nov 2002 21:47:00 +0100

pcsc-perl (1.1.2-3) unstable; urgency=low

  * debian/control: use Build-Depends: perl without version number to allow
    easy backport since it is not mandatory to make the package work.
  * debian/docs: removed since it only installs README.* for installation on
    Unix, Win and MacOSX
  * debian/docs: just contains upstream README

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 29 Aug 2002 18:35:44 +0200

pcsc-perl (1.1.2-2) unstable; urgency=low

  * debian/control: update Standards-Version from 3.5.2 to 3.5.6
  * PCSC.xs: add a pTHX_ cast

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 28 Aug 2002 21:42:53 +0000

pcsc-perl (1.1.2-1) unstable; urgency=low

  * New upstream release
  * Rebuild with Perl 5.8

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 28 Aug 2002 17:51:16 +0000

pcsc-perl (1.1.1-3) unstable; urgency=low

  * debian/control: change priority from optional to extra since libpcsclite0
    which pcsc-perl depends on is also extra (policy section 2.2)
    Closes: #145185

 -- Ludovic Rousseau <rousseau@debian.org>  Tue, 30 Apr 2002 11:41:57 +0200

pcsc-perl (1.1.1-2) unstable; urgency=high

  * urgency=high since it corrects a dependency RC bug found by myself
  * PCSCperl.h: use libpcsclite.so.0 provided by libpcsclite0 instead of
    libpcsclite.so provided by libpcsclite-dev
  * debian/control: depends on libpcsclite0 and not pcsc-ifd-handler
  * debian/dirs: removed since useless
  * debian/control: name the provided Perl libraries to allow to find them by
    name

 -- Ludovic Rousseau <rousseau@debian.org>  Sat,  6 Apr 2002 12:44:21 +0200

pcsc-perl (1.1.1-1) unstable; urgency=low

  * New upstream release
  * removed Changelog from debian/docs
  * changed upstream source from linuxnet.com to ludovic.rousseau.free.fr in
    debian/watch

 -- Ludovic Rousseau <rousseau@debian.org>  Sat,  9 Mar 2002 18:35:06 +0100

pcsc-perl (1.0.8-2) unstable; urgency=low

  * debian/control: change section: from devel to interpreters as requested
    by Debian Installer

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 14 Nov 2001 19:50:20 +0100

pcsc-perl (1.0.8-1) unstable; urgency=low

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 20 Oct 2001 00:17:36 +0200

pcsc-perl (1.0.7-2) unstable; urgency=low

  * Added Changelog in debian/docs
  * Added Provides: libpcsc-card-perl in debian/control

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 17 Oct 2001 23:01:51 +0200

pcsc-perl (1.0.7-1) unstable; urgency=low

  * Initial Release. Closes: Bug#116001

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 17 Oct 2001 22:57:05 +0200
